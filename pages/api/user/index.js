import nc from "next-connect";
import bcrypt from "bcryptjs";
import ErrorHandler from "@/src/handlers/error.handler";
import UserController from "@/src/controller/user.controller";
import {isNumber} from 'lodash'

const handler = nc(ErrorHandler)

handler
    .post(async(req,res)=>{
        let recordData = req.body;

        // check email
        let salt = bcrypt.genSaltSync(10)
        console.log(salt)
        let hashPassword = bcrypt.hashSync(recordData?.password, salt)
        Reflect.set(recordData, 'password', hashPassword)
        Reflect.set(recordData, 'salt', salt)

        // create user baru
        const [err,data] = await new UserController({
            fields: recordData
        }).create()

        if(err){
            return res.status(400).json({
                message: err?.message ?? "Error: Some Error"
            })
        }

        Reflect.deleteProperty(data,'password')
        Reflect.deleteProperty(data,'salt')

        return res.status(200).json({
            message: 'OK!',
            data: data
        })

    })
    .delete(async (req,res)=>{
        let recordData = req.body

        const [err,data] = await new UserController({
            key: recordData?.key ?? 'id',
            value: isNumber(recordData?.value) ? Number(recordData?.value) : recordData?.valueOf() ?? null
        }).delete()

        if(err){
            return res.status(400).json({
                message: err?.message ?? "Error: Some Error"
            })
        }

        return res.status(200).json({
            message: 'OK!',
            data: data
        })
    })

export default handler