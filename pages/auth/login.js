import { useFormik } from 'formik'
import { signIn } from 'next-auth/react'
import * as Yup from 'yup'
import {useRouter} from "next/router";

export default function Login() {
    const router = useRouter();

    const formik = useFormik({
        initialValues: {
            email: '',
            password: ''
        },
        validationSchema: Yup.object({
            email: Yup.string()
                .max(30, 'Must be 30 characters or less')
                .email('Invalid Email Address')
                .required('please enter your email'),
            password: Yup.string()
                .required('please enter your password')
        }),
        onSubmit: async (value) => {
            console.log({ value });
            const credentials = await signIn(
                'credentials',
                {
                    email: value?.email,
                    password: value?.password,
                    redirect: false
                }
            )

            if (credentials.ok){
                router.push(
                    '/'
                )
            }
            // return {
            //     ...credentials
            // }
        }

    })
    return (
        <section className="bg-gray-50 dark:bg-gray-900">
            <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto md:h-screen lg:py-0">
                <a href="#" className="flex items-center mb-6 text-2xl font-semibold text-gray-900 dark:text-white">
                        Farhan Test
                </a>
                <div className="w-full bg-white rounded-lg shadow dark:border md:mt-0 sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700">
                    <div className="p-6 space-y-4 md:space-y-6 sm:p-8">
                        <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
                            Sign in to your account
                        </h1>
                        <form className={'space-y-4 md:space-y-6'} onSubmit={formik.handleSubmit}>
                            <div>
                                <label
                                    className={'block mb-2 text-sm font-medium text-gray-900 dark:text-white'}>
                                    <span>Email</span>
                                </label>
                                <input
                                    className='bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-blue-600 focus:border-blue-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500'
                                        type='email'
                                        name={'email'}
                                        value={formik?.values?.email}
                                        onChange={formik.handleChange}
                                        placeholder={'input your email'}
                                    ></input>
                                    {
                                        formik.errors &&
                                        formik.touched &&
                                        formik.errors?.email &&
                                        formik.touched?.email &&
                                        (
                                            <span className={'!text-red-500 !text-xs'}>{formik.errors?.email}</span>
                                        )
                                    }
                            </div>
                            <div>

                                <label
                                    className={'block mb-2 text-sm font-medium text-gray-900 dark:text-white'}>
                                    <span>Email</span>
                                </label>
                                <input
                                    className='bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-blue-600 focus:border-blue-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500'
                                    type='password'
                                    name={'password'}
                                    value={formik?.values?.password}
                                    onChange={formik.handleChange}
                                    placeholder={'input your password'}
                                ></input>
                                {
                                    formik.errors &&
                                    formik.touched &&
                                    formik.errors?.password &&
                                    formik.touched?.password &&
                                    (
                                        <span className={'!text-red-500 !text-xs'}>{formik.errors?.password}</span>
                                    )
                                }
                            </div>
                            <button
                                type={'submit'}
                                className={'w-full !text-white !bg-blue-600 hover:!bg-blue-700 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800'}
                            >
                                Submit
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    )
};
